import itertools
from typing import Iterable, Iterator


class InexhaustibleGenerator:
    """
    # InexhaustibleGenerator

    exhausts the original generator to
    - save an internal copy for future calls
    - return an equal one every time the object is called
    """

    def __init__(self, iterable: Iterable = None) -> None:
        if iterable is None:
            iterable = tuple()
        self._original: Iterator = iter(iterable)

    def __call__(self) -> Iterator:
        self._original, to_return = itertools.tee(self._original, 2)
        return to_return



class ExtensibleGenerator(InexhaustibleGenerator):
    """
    # ExtensibleGenerator

    implements iterator.append

    ## Note on O(n):
    every addition requires chaining the old generator with
    the new part in order to create the new one: the append
    method takes O(N) and not O(1) like normal lists

    so appending elements one-by-one will result in O(n*m)
    where n is len of the original and m is number of elements added
    """

    def append(self, iterable: Iterable):
        try:
            _new = iter(iterable)
        except TypeError:
            _new = iter((iterable,))
        self._original = itertools.chain(self._original, _new)
