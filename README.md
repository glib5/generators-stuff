# generators stuff

## endless generator

simple class to make a generator never exhaust

nice for **very** long iterables that need to be accessed several times

whole memory usage: 2 generators
- one to use
- one for reference

### example

``` python

import generator_tools

nums = list(i for i in range(5))
infinite_nums = generator_tools.InexhaustibleGenerator(nums)
x = infinite_nums()
# iterator is consumed
print(tuple(x))
# this is empty
print(tuple(x))
x = infinite_nums()
# iterator can be consumed again
print(tuple(x))


# this generator inherits from the previous class
ext = generator_tools.ExtensibleGenerator([1, 2, 3])
# append like to a normal list
for i in [4, 5, 6]:
    ext.append(i)
    print(tuple(ext()))

```
